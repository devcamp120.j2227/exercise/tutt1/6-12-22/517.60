import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';

class Count extends Component {
    constructor(props) {
        super(props)
        this.state = {count: 0}
    }
    upClick = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    downClick = () => {
        this.setState({
            count: this.state.count - 1
        })
    }
    render() {
        return (
            <div className="container text-center">
                <div className="row mt-5">
                    <button className="btn btn-success" onClick={this.upClick}>Up</button>
                </div>
                <div className="row mt-5">
                    <button className="btn btn-danger" onClick={this.downClick}>Down</button>
                </div>
                <div className="row mt-5">
                    <p>Count: {this.state.count}</p>
                </div>
            </div>
        )
    }
}

export default Count